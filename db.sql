DROP TABLE IF EXISTS users;

CREATE TABLE users
(
    login VARCHAR(35) UNIQUE NOT NULL,
    password VARCHAR(50) NOT NULL,
    first_name VARCHAR(40),
    last_name VARCHAR(40),
    address VARCHAR(120),
    email VARCHAR(120),
    phone VARCHAR(20),
    birthday TIMESTAMP,

    CONSTRAINT pk_personne
        PRIMARY KEY (login),
    CONSTRAINT mdp_security_length
        CHECK 
        (
            LENGTH(password) >= 8
        )
);

INSERT INTO users 
VALUES 
('test', 'password', 'Prenom', 'Nom', '42 boulevard du Général Stonks', 'test@noreply.net', '0123456789', '2000-01-01 00:00:00+00');