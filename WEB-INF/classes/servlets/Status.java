package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.UserSession;
import util.ContentType;

@SuppressWarnings("serial")
@WebServlet("/" + Status.SERVLET_PUBLIC_NAME)
public class Status extends HttpServlet {
	public static final String SERVLET_PUBLIC_NAME = "status";

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType(ContentType.TEXT_HTML_UTF_8);
		
		final PrintWriter out = resp.getWriter();
		
		html(req, resp, out);
	}

	private void html(HttpServletRequest req, HttpServletResponse resp, PrintWriter out) {
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		htmlHead(req, resp, out);
		htmlBody(req, resp, out);
		out.println("</html>");
	}

	private void htmlHead(HttpServletRequest req, HttpServletResponse resp, PrintWriter out) {
		out.println("<head>");
		out.println("<meta charset=\"UTF-8\"/>");
		out.println("<title>Status</title>");
		out.println("</head>");
	}

	private void htmlBody(HttpServletRequest req, HttpServletResponse resp, PrintWriter out) {
		final HttpSession httpSession = req.getSession(true);
		UserSession session = (UserSession)httpSession.getAttribute("user");
		out.println("<body>");
		out.println(String.format("</h1>L'utilisateur %s %s</h1>", session.username, session.getUserExists() ? "existe" : "n'existe pas"));
		out.println("</body>");
	}
}
