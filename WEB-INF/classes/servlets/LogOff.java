package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet("/" + LogOff.SERVLET_PUBLIC_NAME)
public class LogOff extends HttpServlet {
	public static final String SERVLET_PUBLIC_NAME = "logoff";
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession(true).invalidate();
		resp.sendRedirect("login.html");
	}
}
