package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public abstract class LoginServlet extends HttpServlet {
	protected void assertIsAuthenticated(HttpServletRequest req, HttpServletResponse resp, ServiceMethod onSuccess, ServiceMethod onFailure) throws ServletException, IOException {
		if (isAuthenticated(req)) {
			onSuccess.perform(req, resp);
		} else {
			onFailure.perform(req, resp);
		}
	}
	
	protected abstract boolean isAuthenticated(HttpServletRequest req) throws ServletException, IOException;
	
	@FunctionalInterface
	protected interface ServiceMethod {
		public void perform(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException;
	}
}