package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.SQLProvider;
import model.UserSession;

@SuppressWarnings("serial")
@WebServlet("/" + Auth.SERVLET_PUBLIC_NAME)
public class Auth extends LoginServlet {
	public static final String SERVLET_PUBLIC_NAME = "authentication";
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		assertIsAuthenticated(req, resp, this::serviceRedirect, this::serviceRedirect);
	}
	
	protected void serviceRedirect(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.sendRedirect(Status.SERVLET_PUBLIC_NAME);
	}
	
	@Override
	protected boolean isAuthenticated(HttpServletRequest req) throws ServletException, IOException {
		final String SQL_QUERY = "SELECT 'success' AS status FROM users WHERE login = ? AND password = ?";
		
		final String
		username = req.getParameter("username"),
		password = req.getParameter("password");
		
		final HttpSession session = req.getSession(true);
		boolean authenticated = false;
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet results = null;
		try {
			connection = SQLProvider.instance().getConnection();
			statement = connection.prepareStatement(SQL_QUERY);
			statement.setString(1, username);
			statement.setString(2, password);
			results = statement.executeQuery();
			
			if (results.next()) {
				authenticated = true;
			}
		} catch(SQLException sqlExc) {
			throw new ServletException(sqlExc);
		}finally {
			try {
				connection.close();
				statement.close();
				results.close();
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		UserSession user = new UserSession(username);
		user.setUserExists(authenticated);
		session.setAttribute("user", user);
		
		return authenticated;
	}
}
