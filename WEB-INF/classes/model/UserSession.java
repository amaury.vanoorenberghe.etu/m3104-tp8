package model;

public class UserSession {
	public static final UserSession NO_SESSION = new UserSession("###") {
		@Override
		public void setUserExists(boolean userExists) {}
	};
	
	public final String username;
	public boolean userExists;
	
	public UserSession(String username) {
		this.username = username;
	}

	public boolean getUserExists() {
		return userExists;
	}

	public void setUserExists(boolean userExists) {
		this.userExists = userExists;
	}
}
